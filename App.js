
import React, { Component } from 'react';
import {
  Platform, StyleSheet, Text, View,
  Button, TextInput, Alert, TouchableOpacity, TouchableHighlight, Image, AsyncStorage,
  Dimensions, CheckBox
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons'

// import { CheckBox } from 'react-native-elements'
import SignUp from './src/SignUp'
import Welcome from './src/Welcome'
import MotelList from './src/MotelList'
import CreateHouse from './src/CreateHouse'
import CreateRoom from './src/CreateRoom'
import BaseStyle from './src/styles/baseStyles'
import Header from './src/Header'
import { NavigationActions } from 'react-navigation'

import {
  createAppContainer,
  createStackNavigator,
} from 'react-navigation';

var width = Dimensions.get('window').width;
const logo = require('./assets/Swarm.png');


class Login extends Component {
  static navigationOptions = {
    header: null
  }
  constructor(props) {
    super(props);
    this.state = {
      phoneNumber: '',
      password: '',
      checked: false
    }
  }

  _onPressSignUp() {
    navigate.navigate('SignUp');
  }

  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={[BaseStyle.container, styles.container]}>
        <View style={BaseStyle.container}>
          <View style={BaseStyle.imageWrapper}>
            <Image source={logo} style={BaseStyle.image} />
          </View>

          <TextInput style={BaseStyle.input} placeholder="So dien thoai" />
          <TextInput style={BaseStyle.input} placeholder="Mat khau" secureTextEntry={true} />

          <TouchableOpacity activeOpacity={0.5} keyBoardShouldPersisTab={true} onPress={this.navigateToScreen('MotelList')}>
            <View style={BaseStyle.button}>
              <Text style={BaseStyle.buttonText}>Đăng nhập</Text>
            </View>
          </TouchableOpacity>

          <View style={BaseStyle.wrapper}>
            <View style={BaseStyle.checkbox}>
              <CheckBox
                checked={this.state.checked}
              />
              <Text>Ghi nhớ mật khẩu</Text>
            </View>
            <TouchableOpacity activeOpacity={0.5} keyBoardShouldPersisTab={true}>
              <View>
                <Text style={styles.forgotPasswordText}>Khôi phục mật khẩu</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>

        <View style={{ marginBottom: 40 }}>
          <View>
            <Text style={styles.label}>Bạn chưa có tài khoản</Text>
          </View>
          <TouchableOpacity activeOpacity={0.5} keyBoardShouldPersisTab={true} onPress={() => navigate('SignUp')}>
            <View>
              <Text style={styles.label}>Đăng kí</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const MainStack = createStackNavigator({
  Login: {
    screen: Login
  },
  SignUp: {
    screen: SignUp
  },
  Welcome: {
    screen: Welcome
  },
  MotelList: {
    screen: MotelList
  },
  CreateRoom: {
    screen: CreateRoom
  }
})

const App = createAppContainer(MainStack);
export default App;


const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 15,
  },
  forgotPasswordText: {
    color: '#333333',
    textAlign: 'right',
    marginTop: 7
  },
  label: {
    fontSize: 14,
    color: '#333333',
    alignSelf: 'center'
  }
});
