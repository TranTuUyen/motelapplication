import React, { Component } from 'react'
import { StyleSheet, View, Text, TextInput, TouchableOpacity, Image, ScrollView, KeyboardAvoidingView } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import BaseStyle from './styles/baseStyles'

const house = require('./../assets/house.jpg')

export default class CreateRoom extends Component {
    static navigationOptions = ({ navigation }) => ({
        headerTitleStyle: { alignSelf: 'center', textAlign: 'center', flexGrow: 1, },
        title: 'Tạo phòng',
        headerRight: (
            <TouchableOpacity onPress={() => { navigation.goBack() }}>
                <Text style={BaseStyle.headerButton}>Hủy</Text>
            </TouchableOpacity>
        )
    })

    render() {
        return (
            <ScrollView contentContainerStyle={styles.container}>
                <KeyboardAvoidingView>
                    <View styles={[styles.container]}>
                        <View style={styles.imageOutline} >
                            {/* <Image style={styles.image} source={house}></Image> */}
                            <View style={styles.imageCircle}>
                                <Icon name='ios-camera' style={{ backgroundColor: 'transparent' }} size={30} />
                            </View>
                            <Text>Tải ảnh phòng</Text>
                        </View>
                        <View style={styles.inputGroup}>
                            <TextInput style={BaseStyle.input} placeholder='Tên phòng' />
                            <TextInput style={BaseStyle.input} placeholder='Phí thuê' />
                            <TextInput style={BaseStyle.input} placeholder='Diện tích' />
                            <TextInput style={BaseStyle.input} keyboardType='numeric' placeholder='Số người tối đa' />
                        </View>

                        <TouchableOpacity style={BaseStyle.button}>
                            <Text style={BaseStyle.buttonText}>Tiếp</Text>
                        </TouchableOpacity>
                    </View>

                </KeyboardAvoidingView>
            </ScrollView>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 15,
    },
    imageOutline: {
        marginHorizontal: 40,
        height: 140,
        marginBottom: 50,
        marginTop: 50,
        borderColor: '#d1d1d1',
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    inputGroup: {
        marginBottom: 80
    },
    imageCircle: {
        borderColor: '#d1d1d1',
        borderWidth: 1,
        borderRadius: 100,
        width: 60,
        height: 60,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 5
    },
    image: {
        maxWidth: '100%',
        maxHeight: '100%'
    }
})