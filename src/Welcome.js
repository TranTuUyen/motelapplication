import React, { Component } from 'react';
import { View, Image, TouchableOpacity, StyleSheet, Text } from 'react-native'
import BaseStyle from './styles/baseStyles'

const logo = require('./../assets/Swarm.png')
const titleFontSize = 22;
export default class Welcome extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={{ alignItems: 'center' }}>
                    <Text style={styles.title}>Đăng kí thành công</Text>
                    <Text>Chào mưng bạn đến với</Text>
                    <View style={BaseStyle.imageWrapper}>
                        <Text style={styles.appName}>MotelApp</Text>
                        <Image style={BaseStyle.image} source={logo} />
                    </View>
                </View>

                <TouchableOpacity>
                    <View style={BaseStyle.button}>
                        <Text style={BaseStyle.buttonText}>Bắt đầu</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        padding: 15
    },
    title: {
        fontSize: titleFontSize,
        fontWeight: 'bold',
        lineHeight: 1.375 * titleFontSize,
        marginBottom: 5
    },
    des: {

    },
    appName: {
        fontSize: 20,
        color: '#ffcc00',
        fontWeight: 'bold',
        marginBottom: 10
    },

})