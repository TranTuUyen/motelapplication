import React from 'react'

import Icon from 'react-native-vector-icons/Ionicons'

const tabBarIcon = name => ({tintColor}) => (
    <Icon name={name} style={{backgroundColor: 'transparent'}} size={24} color={tintColor} />
)

export default tabBarIcon;