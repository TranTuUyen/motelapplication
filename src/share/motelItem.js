import React from 'react'
import {StyleSheet, Text,Image, View} from 'react-native'


const logo = require('./../../assets/Swarm.png')

export default function MotelItem({image,name,roomNumber,location}) {
    return (
        <View style={styles.itemWrapper}>
            <Image source={logo} style={styles.image}></Image>
            <View style={styles.info}>
                <Text>{name}</Text>
                <Text>{location}</Text>
                <Text>{roomNumber} phòng</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    itemWrapper: {
        flexDirection: 'row',
        paddingVertical: 5,
        paddingHorizontal: 10,
        borderBottomWidth: 1,
        borderColor: '#d2d2d2',
        marginHorizontal: 15 
    },
    image: {
        width: 80,
        height: 80
    },
    info: {
        flex: 1,
        flexDirection: 'column',
        paddingLeft: 10,
        marginTop: 5
    }
})