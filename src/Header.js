import React, { Component } from 'react';
import {
    Platform, StyleSheet, Text, View,
    Button, TextInput, Alert, TouchableOpacity, TouchableHighlight, Image, AsyncStorage,
    Dimensions
} from 'react-native';
import BaseStyle from './styles/baseStyles'
import baseStyles from './styles/baseStyles';

export default class Header extends Component {
    render() {
        let nextScreen = this.props.nextScreen;
        return (
            <View>
                <TouchableOpacity onPress={() => {this.props.navigation.navigate(nextScreen)}}>
                    <Text style={baseStyles.headerButton}>Tạo nhà</Text>
                </TouchableOpacity>
            </View>
        )
    }
}