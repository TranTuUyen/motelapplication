import React, { Component } from 'react';
import {
    Platform, StyleSheet, Text, View,
    Button, TextInput, Alert, TouchableOpacity, TouchableHighlight, Image, AsyncStorage,
    Dimensions, FlatList
} from 'react-native';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs'
import MotelItem from './share/motelItem'
import BaseStyle from './styles/baseStyles'
import TabBarIcon from './share/tabBarIcon'
import Header from './../src/Header'
import CreateHouse from './CreateHouse'
import {
    createAppContainer,
    createStackNavigator,
} from 'react-navigation';
import { NavigationActions } from 'react-navigation';

/**
 * có 2 header, 1 header của react-navigation-material-bottom-tabs và 1 header của react-navigation
 * header bên ngoài là do các màn hình đang bọc bởi hàm createMaterialBottomTabNavigator()
 * header trong là của createStackNavigator
 * 
 * có 2 giải pháp
 * 
 * c1: bỏ header của createMaterialBottomTabNavigator (làm theo cách này nhé)
 * 
 * c2: bỏ header của createStackNavigator
 * với cách 2 cần 1 nơi global để lưu các trạng thái thay đổi ví dụ như title của header, khi thay đổi thì set lại cái title,
 * có thể tạo 1 file global hoặc sử dụng redux.
 */

//  không cần build lại mỗi khi mở lại app đâu
// chạy react-native start or npm start là được

class Building extends Component {
    // static navigationOptions = {
    //     header: null
    // }

    static navigationOptions = ({ navigation }) => ({
        headerTitleStyle: { alignSelf: 'center', textAlign: 'center', flexGrow: 1, },
        title: 'Danh sách tòa nhà',
        headerRight: (
            <Header navigation={navigation} nextScreen='CreateHouse' />
        )
    })

    render() {
        var list = [
            {
                key: '1',
                name: 'Nha 1',
                location: "Ha Dong, Ha Noi",
                roomNumber: 12
            },
            {
                key: '2',
                name: 'Nha 1',
                location: "Ha Dong, Ha Noi",
                roomNumber: 12
            },
            {
                key: '3',
                name: 'Nha 1',
                location: "Ha Dong, Ha Noi",
                roomNumber: 12
            },
            {
                key: '4',
                name: 'Nha 1',
                location: "Ha Dong, Ha Noi",
                roomNumber: 12
            },
            {
                key: '5',
                name: 'Nha 1',
                location: "Ha Dong, Ha Noi",
                roomNumber: 12
            },
            {
                key: '6',
                name: 'Nha 1',
                location: "Ha Dong, Ha Noi",
                roomNumber: 12
            },
            {
                key: '7',
                name: 'Nha 1',
                location: "Ha Dong, Ha Noi",
                roomNumber: 12
            },
            {
                key: '8',
                name: 'Nha 1',
                location: "Ha Dong, Ha Noi",
                roomNumber: 12
            }
        ]
        return (
            <View style={[BaseStyle.container, styles.container]}>
                <FlatList
                    data={list}
                    renderItem={({ item }) => <MotelItem name={item.name} location={item.location} roomNumber={item.roomNumber} />}
                />
            </View>
        )
    }
}

class Room extends Component {
    static navigationOptions = {
        title: 'Buiding',
        tabBarIcon: TabBarIcon("ios-medkit")

    }
    render() {
        var list = [
            {
                key: '1',
                name: 'Toa nha 1',
                location: "Ha Dong, Ha Noi",
                roomNumber: 10
            }
        ];
        return (
            <View style={[BaseStyle.container, styles.container]}>
                <FlatList
                    data={list}
                    renderItem={({ item }) => <MotelItem name={item.name} location={item.location} roomNumber={item.roomNumber} />}
                />
            </View>
        )
    }
}

class Finance extends Component {
    static navigationOptions = {
        title: 'Finace',
        tabBarIcon: TabBarIcon("ios-heart")
    }
    render() {
        return (
            <View style={[BaseStyle.container, styles.container]}>

            </View>
        )
    }
}

class Trouble extends Component {
    static navigationOptions = {
        title: 'Sự cố',
        tabBarIcon: TabBarIcon("ios-medkit"),
    }
    render() {
        return (
            <View style={[BaseStyle.container, styles.container]}>
                <FlatList>
                    <View style={styles.itemBox}>
                        <View style={styles.itemHeader}>
                            <View style={styles.itemHeaderIcon}></View>
                            <View style={styles.itemHeaderText}>
                                <Text style={styles.itemHeaderTextTitle}>Trần Tú Uyên</Text>
                                <View style={styles.hContainer}>
                                    <Text>Tòa nhà: A</Text>
                                    <Text>Phòng A202</Text>
                                    <Text>01/01/2019</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </FlatList>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 5,
    },
    itemBox: {
        borderWidth: 1,
        borderColor: '#d2d2d2',
        padding: 10
    },
    hContainer: {
        flexDirection: 'row',
    },
    itemHeader: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    itemHeaderIcon: {
        width: 30,
        height: 30,
        backgroundColor: '#666666',
        marginRight: 8,
        borderRadius: 100,
        overflow: 'hidden'
    },
    itemHeaderText: {
        flex: 1
    },
    itemHeaderTextTitle: {
        fontSize: 18
    },
    itemHeaderTextDes: {
    }
})

const houseStack = createStackNavigator({
    Building: Building,
    CreateHouse: CreateHouse
})

houseStack.navigationOptions = {
    title: 'MotelList',
    tabBarIcon: TabBarIcon("ios-folder")
}
const abc = createMaterialBottomTabNavigator(
    {
        houseStack,
        Room,
        Finance,
        Trouble
    },
    {
        initialRouteName: 'houseStack',
        activeColor: '#333333',
        inactiveColor: '#999999',
        barStyle: { backgroundColor: '#ffffff' },
    }
)


abc.navigationOptions = ({ navigation }) => {
    const { routeName } = navigation.state.routes[navigation.state.index]; //This gives current route
    switch (routeName) {
        case "houseStack":
            header = null
            // headerTitle = "Danh sách tòa nhà";
            // headerRight = (<Header navigation={navigation} nextScreen='CreateHouse' />)
            break;
        case "Room":
            // headerTitle = "Danh sách phòng";
            // headerRight = null;
            break;
        case "Finance":
            // headerTitle = "Tài chính";
            // headerRight = null;
            break;
        case "Trouble":
            // headerTitle = "Danh sách sự cố";
            // headerRight = null;
            break;
    }
    return {
        header: header
    }
    // return {
    //     headerTitle: headerTitle,
    //     headerRight: headerRight
    // }
}

export default abc;