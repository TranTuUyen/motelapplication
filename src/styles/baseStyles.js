import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        // alignItems: 'center',
        backgroundColor: '#F5FCFF',
        // paddingHorizontal: 15,
    },
    wrapper: {
        flexDirection: 'row'
    },
    checkboxWrapper: {
        flex: 1,
    },
    imageWrapper: {
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'center'
    },
    image: {
        height: 100,
        width: 100,
        marginBottom: 50,
    },
    input: {
        height: 40,
        marginVertical: 5,
        borderColor: '#d1d1d1',
        borderWidth: 1,
        borderRadius: 4,
        padding: 10
    },
    button: {
        backgroundColor: "#d73352",
        paddingVertical: 8,
        marginVertical: 8,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 4
    },
    buttonText: {
        fontSize: 16,
        color: '#FFFFFF',
        textAlign: 'center',
    },
    checkbox: {
        flex: 1, color: '#333333',
        flexDirection: 'row',
        alignItems: 'center',
    },
    headerButton: {
        fontSize: 14,
        color: '#333333',
        marginRight: 10
      }
})