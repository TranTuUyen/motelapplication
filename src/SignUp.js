
import React, { Component } from 'react';
import {
    Platform, StyleSheet, Text, View,
    Button, TextInput, Alert, TouchableOpacity, TouchableHighlight, Image, AsyncStorage,
    Dimensions
} from 'react-native';
import BaseStyle from './styles/baseStyles'

export default class SignUp extends Component {
    static navigationOptions = {
        title: 'Sign up',
    };


    _onPressSignUp() {
        this.props.navigation.navigate('Welcome');
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={BaseStyle.container}>
                <TextInput style={BaseStyle.input} placeholder='Họ và tên' />
                <TextInput style={BaseStyle.input} placeholder='Số điện thoại' />
                <TextInput style={BaseStyle.input} placeholder='Nhập mật khẩu' />
                <TextInput style={BaseStyle.input} placeholder='Nhập lại mật khẩu' />
                <TouchableOpacity activeOpacity={0.5} onPress={() => navigate('Welcome')}>
                    <View style={BaseStyle.button}>
                        <Text style={BaseStyle.buttonText}>Đăng kí</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
   
})